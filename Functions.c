#include <stdlib.h>
#include <stdio.h>
#include "life.h"

FILE *Openfile(void){
	FILE *fp;
	fp = fopen("input6x6.txt", "r");		//open input file
	if(fp == NULL) {
      	printf("Error in opening file\n");
      	
	}
	return fp;
}






int generation(int **Life,/* int **sublifeprv, int *sameState,*/ int **Neighbors, int Sizen, int Sizem, int Corners[4], int *Upper, int *Down, int *Left, int *Right){
	int currentstate;
	int i,j;
	//do{
		currentstate=0;
		
		int a,b;

		for(i=0; i<Sizen; i++){
			for(j=0; j<Sizem; j++){
				if(j==0){
					if(i==0){
						Neighbors[i][j] = Corners[0] + Upper[j] + Upper[j+1] + Life[i][j+1] + Life[i+1][j+1] + Life[i+1][j] + Left[i+1] + Left[i];
					}
					else if(i<(Sizen-1)){
						Neighbors[i][j] = Left[i-1] + Life[i-1][j] + Life[i-1][j+1] + Life[i][j+1] + Life[i+1][j+1] + Life[i+1][j] + Left[i+1] + Left[i];
					}
					else{
						Neighbors[i][j] = Left[i-1] + Life[i-1][j] + Life[i-1][j+1] + Life[i][j+1] + Down[j+1] + Down[j] + Corners[3] + Left[i];
					}
				}
				else if(j<(Sizem-1)){
					if(i==0){
						Neighbors[i][j] = Upper[j-1] + Upper[j] + Upper[j+1] + Life[i][j+1] + Life[i+1][j+1] + Life[i+1][j] + Life[i+1][j-1] + Life[i][j-1];
					}
					else if(i<(Sizen-1)){
						Neighbors[i][j] = Life[i-1][j-1] + Life[i-1][j] + Life[i-1][j+1] + Life[i][j+1] + Life[i+1][j+1] + Life[i+1][j] + Life[i+1][j-1] + Life[i][j-1];
					}
					else{
						Neighbors[i][j] = Life[i-1][j-1] + Life[i-1][j] + Life[i-1][j+1] + Life[i][j+1] + Down[j+1] + Down[j] + Down[j-1] + Life[i][j-1];
					}
				}
				else{
					if(i==0){
						Neighbors[i][j] = Upper[j-1] + Upper[j] + Corners[1] + Right[i] + Right[i+1] + Life[i+1][j] + Life[i+1][j-1] + Life[i][j-1];
					}
					else if(i<(Sizen-1)){
						Neighbors[i][j] = Life[i-1][j-1] + Life[i-1][j] + Right[i-1] + Right[i] + Right[i+1] + Life[i+1][j] + Life[i+1][j-1] + Life[i][j-1];
					}
					else{
						Neighbors[i][j] = Life[i-1][j-1] + Life[i-1][j] + Right[i-1] + Right[i] + Corners[3] + Down[j] + Down[j-1] + Life[i][j-1];

					}

				}
			}
		}


		
		for(i=0;i<Sizen;i++) {
			for(j=0;j<Sizem;j++){
			
				if( Life[i][j] == 1 && Neighbors[i][j] <= 1){	//1st rule
					Life[i][j] = 0;
					currentstate =1;												//to currentstate edw deixnei oti o pinakas telika metavlhthhke
				}
				else if( Life[i][j] == 1 && Neighbors[i][j] >= 4){	//3rd rule
					Life[i][j] = 0;
					currentstate =1;
				}
				else if( Life[i][j] == 0 && Neighbors[i][j] == 3){	//4th rule
					Life[i][j] = 1;
					currentstate =1;
				}
				
			}
		}
		if(currentstate==1){  //elegxos an ola einai 0
			int sum=0;
			for (i = 0; i < Sizen; i++) {
				for(j=0;j<Sizem;j++){
					// if(Life[i][j] != sublifeprv[i][j]){
					// 	*sameState = 1;
					// 	break;
					// }
					
					sum = sum + Life[i][j];
				}
			}
			if(sum==0){
				currentstate=0;
			}
		}


	//}while(currentstate == 0);  //**************************************************gt prepei na uparxei while?

	return currentstate;
}



int malloc2darray(int ***array, int sizen, int sizem) {

    /* allocate the n*m contiguous items */
    int *p = (int *)malloc(sizen*sizem*sizeof(int));
    if (!p){ 
    	return -1;
    }

    /* allocate the row pointers into the memory */
    (*array) = (int **)malloc(sizen*sizeof(int*));
    if (!(*array)) {
       free(p);
       return -1;
    }

    /* set up the pointers into the contiguous memory */
    for (int i=0; i<sizen; i++) 
       (*array)[i] = &(p[i*sizem]);

    return 0;
}

int free2darray(int ***array){
    /* free the memory - the first element of the array is at the start */
    free(&((*array)[0][0]));

    /* free the pointers into the memory */
    free(*array);

    return 0;
}