#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include "life.h"
#include "Functions.h"
#include "mpi.h"


#define UP    0
#define UP_RIGHT 4
#define UP_LEFT 7
#define DOWN  1
#define DOWN_LEFT 5
#define DOWN_RIGHT 3
#define LEFT  6
#define RIGHT 2

#define SIZEN 2520  //81 420 3200 2520
#define SIZEM 2520  //81 420 3200 2520
#define LOOPS 2000

#define N 100

int main(int argc, char **argv){
	int curRound=0;
    int i,j, numprocs, rank;
    FILE *fp;
    MPI_Status recv_status, send_status;
    MPI_Request request;


    int currState;  //elegxei an sto plegma den uparxei pia zwh
    int mpiReduceRes = 0;
    int numOfN = 1;

    MPI_Init(&argc, &argv);    //initializing mpi
    double t1, t2; 
    
    t1 = MPI_Wtime(); 

    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);    //getting the number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);    //getting the id of each process
    //printf("numprocs is %d\n", numprocs);
    

	//orismos pinaka
    int **Life;
    if(rank==0){      
    	malloc2darray(&Life,SIZEN,SIZEM);
	}


    //upologismos tou megethous ths pleuras ths topologias
    int topologysize;
    topologysize = sqrt(numprocs);

    int source, dest, tag=1;
    int nbrs[8], dims[2]={topologysize,topologysize};
    int periods[2]={1,1}, reorder=1, coords[2], diagonal_coords[2];

    MPI_Request reqs[16];
    MPI_Status stats[16];
    MPI_Comm cartcomm;   // required variable


    
      // create cartesian virtual topology, get rank, coordinates, neighbor ranks
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &cartcomm);
    MPI_Comm_rank(cartcomm, &rank);
    MPI_Cart_coords(cartcomm, rank, 2, coords);
    MPI_Cart_shift(cartcomm, 0, 1, &nbrs[UP], &nbrs[DOWN]);
    MPI_Cart_shift(cartcomm, 1, 1, &nbrs[LEFT], &nbrs[RIGHT]);


    //vriskw tous diagwnious geitones
    diagonal_coords[0] = coords[0]-1;
    diagonal_coords[1] = coords[1]-1;
    MPI_Cart_rank(cartcomm,diagonal_coords,&nbrs[UP_LEFT]);

    diagonal_coords[0] = coords[0]-1;
    diagonal_coords[1] = coords[1]+1;
    MPI_Cart_rank(cartcomm,diagonal_coords,&nbrs[UP_RIGHT]);

    diagonal_coords[0] = coords[0]+1;
    diagonal_coords[1] = coords[1]-1;
    MPI_Cart_rank(cartcomm,diagonal_coords,&nbrs[DOWN_LEFT]);

    diagonal_coords[0] = coords[0]+1;
    diagonal_coords[1] = coords[1]+1;
    MPI_Cart_rank(cartcomm,diagonal_coords,&nbrs[DOWN_RIGHT]);

    //printf("rank= %d coords= %d %d  neighbors(ul,u,ur,dl,d,dr,l,r)=%d %d %d %d %d %d %d %d\n",rank,coords[0],coords[1],nbrs[UP_LEFT],nbrs[UP],nbrs[UP_RIGHT],nbrs[DOWN_LEFT],nbrs[DOWN],nbrs[DOWN_RIGHT],nbrs[LEFT],nbrs[RIGHT]);

    int **subLife; //orismos pinaka subLife

    
    int z=0,t=0;
	int subsize[] = {SIZEN / topologysize,SIZEM / topologysize};
	int bigsize[] ={SIZEN,SIZEM};
    int **Neighbors;
    malloc2darray(&Neighbors,subsize[0],subsize[1]);
    for(i=0;i<subsize[0];i++) {
        for(j=0;j<subsize[1];j++){
            Neighbors[i][j] = 0;          //arxikopoihsh geitonwn  //svhsimo prin thn paradwsh
        }
    }
	int starts[] = {0,0};
	MPI_Datatype subarray;
	malloc2darray(&subLife,subsize[0],subsize[1]);

    int sameState = 0;//elegxos an to plegma menei idio gia duo sunexomenes genies
    int **subLifeprv;
    malloc2darray(&subLifeprv,subsize[0],subsize[1]);
    

    


    //orizw ta stoixeia p tha labw apo kathe geitona
    int *Corners;
    int *Upper;
    int *Down;
    int *Left;
    int *Right;
    Corners = (int*)malloc(4*sizeof(int));
    Upper = (int*)malloc(subsize[1]*sizeof(int));
    Down = (int*)malloc(subsize[1]*sizeof(int));
    Left = (int*)malloc(subsize[0]*sizeof(int));
    Right = (int*)malloc(subsize[0]*sizeof(int));
    int subLife_subsize[2];

    int loop;
    for (loop = 0; loop < LOOPS; loop++){
    	if(curRound==0){  //master process
            curRound = 1;

            //orizw to subarray se kathe process
            MPI_Status stat;
            MPI_Request prs[numprocs];

            if(rank==0){
                //diavasma apo arxeio

                /*fp = Openfile(); //opening the file
                if (!fp){
                    printf("error opening file\n");
                    MPI_Finalize();
                    return -1;
                }
            

                for(i=0;i<SIZEN;i++) {
                    for(j=0;j<SIZEM;j++){
                        //Neighbors[i][j] = 0;          //arxikopoihsh geitonwn  //svhsimo prin thn paradwsh
                        fscanf(fp, "%d ", &Life[i][j]);   //read from file to array
                    }
                }*/
                ////////////////////////////

                //random dedomena
                
                srand(time(NULL));   // should only be called once
                for(i=0;i<SIZEN;i++) {
                    for(j=0;j<SIZEM;j++){
                        //Neighbors[i][j] = 0;          //arxikopoihsh geitonwn  //svhsimo prin thn paradwsh
                        Life[i][j] = rand() % 2;    //read from file to array
                    }
                }
                
                ////////////////////////


                //stelnw to subarray se kathe proces
                dest=0;
                for(i=0; i<topologysize; i++){
                    starts[0] = i*subsize[0];

                    for(j=0; j<topologysize; j++){   
                    
                        starts[1] = j*subsize[1];
                        MPI_Type_create_subarray(2, bigsize, subsize, starts, MPI_ORDER_C,MPI_INT, &subarray);  
                        MPI_Type_commit(&subarray);

                        MPI_Isend(&(Life[0][0]), 1, subarray, dest, tag, cartcomm, &prs[dest]);
                        MPI_Type_free(&subarray);
                        dest++;
                    }
                }

                MPI_Irecv(&(subLife[0][0]), subsize[0]*subsize[1], MPI_INT, 0, tag, cartcomm, &prs[dest]);
            }
            else{
                MPI_Recv(&(subLife[0][0]), subsize[0]*subsize[1], MPI_INT, 0, tag, cartcomm, &stat);
            }

        }

        // int r=0,q=0;                            //deutereuon pinakas gia elegxo idias katastashs. pros to parwn den xreiazetai
        // for(r=0; r<subsize[0]; r++){
        //     for(q=0; q<subsize[1]; q++){
        //         subLifeprv[r][q] = subLife[r][q];
        //     }
        // }

        for(i=0; i<8; i++){
        	if(i==0){
             	subLife_subsize[0] = 1;
             	subLife_subsize[1] = subsize[1];

             	starts[0] = 0;
             	starts[1] = 0;
             	MPI_Type_create_subarray(1, subsize, subLife_subsize, starts, MPI_ORDER_C,MPI_INT, &subarray);  
                MPI_Type_commit(&subarray);
            }
            else if(i==2){
             	subLife_subsize[0] = subsize[0];
             	subLife_subsize[1] = 1;

             	starts[0] = 0;
             	starts[1] = subsize[1] - 1;
             	MPI_Type_create_subarray(1, subsize, subLife_subsize, starts, MPI_ORDER_C,MPI_INT, &subarray);  
                MPI_Type_commit(&subarray);
            }
            else if(i==4){
             	subLife_subsize[0] = 1;
             	subLife_subsize[1] = subsize[1];

             	starts[0] = subsize[0] - 1;
             	starts[1] = 0;
             	MPI_Type_create_subarray(1, subsize, subLife_subsize, starts, MPI_ORDER_C,MPI_INT, &subarray);  
                MPI_Type_commit(&subarray);
            }
            else if(i==6){
             	subLife_subsize[0] = subsize[0];
             	subLife_subsize[1] = 1;

             	starts[0] = 0;
             	starts[1] = 0;
             	MPI_Type_create_subarray(1, subsize, subLife_subsize, starts, MPI_ORDER_C,MPI_INT, &subarray);  
                MPI_Type_commit(&subarray);
            }
             	
                
            if(i == 0){
                dest = nbrs[UP];
                source = nbrs[UP];
                MPI_Isend(&(subLife[0][0]), 1, subarray, dest, tag + 1, cartcomm, &reqs[i]);  //stelnw ston panw
                MPI_Irecv(&Upper[0], subsize[1], MPI_INT, source, tag + 9, cartcomm, &reqs[i+8]);  //lamvanw apo ton panw
            }
            else  if(i == 1){
                dest = nbrs[UP_RIGHT];
                source = nbrs[UP_RIGHT];
                MPI_Isend(&(subLife[0][subsize[1] - 1]), 1, MPI_INT, dest, tag + 3, cartcomm, &reqs[i]);  //stelnw ston panw deksia
                MPI_Irecv(&Corners[1], 1, MPI_INT, source, tag + 11, cartcomm, &reqs[i+8]);  //lamvanw apo ton panw deksia
            }
            else if(i == 2){
                dest = nbrs[RIGHT];
                source = nbrs[RIGHT];
                MPI_Isend(&(subLife[0][0]), 1, subarray, dest, tag + 5, cartcomm, &reqs[i]);  //stelnw ston deksia
                MPI_Irecv(&Right[0], subsize[0], MPI_INT, source, tag + 13, cartcomm, &reqs[i+8]);  //lamvanw apo ton deksia
            }
            else if(i == 3){
                dest = nbrs[DOWN_RIGHT];
                source = nbrs[DOWN_RIGHT];
                MPI_Isend(&(subLife[subsize[0] - 1][subsize[1] - 1]), 1, MPI_INT, dest, tag + 7, cartcomm, &reqs[i]);  //stelnw ston katw deksia
                MPI_Irecv(&Corners[2], 1, MPI_INT, source, tag + 15, cartcomm, &reqs[i+8]);  //lamvanw apo ton katw deksia
            }
            else if(i == 4){
                dest = nbrs[DOWN];
                source = nbrs[DOWN];
                MPI_Isend(&(subLife[0][0]), 1, subarray, dest, tag + 9, cartcomm, &reqs[i]);  //stelnw ston katw
                MPI_Irecv(&Down[0], subsize[1], MPI_INT, source, tag + 1, cartcomm, &reqs[i+8]);  //lamvanw apo ton katw
            }
            else if(i==5){
                dest = nbrs[DOWN_LEFT];
                source = nbrs[DOWN_LEFT];
                MPI_Isend(&(subLife[subsize[0] - 1][0]), 1, MPI_INT, dest, tag + 11, cartcomm, &reqs[i]);  //stelnw ston katw aristera
                MPI_Irecv(&Corners[3], 1, MPI_INT, source, tag + 3, cartcomm, &reqs[i+8]);  //lamvanw apo ton katw aristera
            }
            else if(i==6){
                dest = nbrs[LEFT];
                source = nbrs[LEFT];
                MPI_Isend(&(subLife[0][0]), 1, subarray, dest, tag +13, cartcomm, &reqs[i]);  //stelnw ston aristera
                MPI_Irecv(&Left[0], subsize[0], MPI_INT, source, tag + 5, cartcomm, &reqs[i+8]);  //lamvanw apo ton aristera
            }
            else if(i==7){
                dest = nbrs[UP_LEFT];
                source = nbrs[UP_LEFT];
                MPI_Isend(&(subLife[0][0]), 1, MPI_INT, dest, tag + 15, cartcomm, &reqs[i]);  //stelnw ston panw aristera
                MPI_Irecv(&Corners[0], 1, MPI_INT, source, tag + 7, cartcomm, &reqs[i+8]);  //lamvanw apo ton panw aristera
            }
                
            if(i==0 || i==2 || i==4 || i==6){
                MPI_Type_free(&subarray);
            }
        }

        
        MPI_Waitall(16, reqs, stats);
        currState = generation(subLife,/* subLifeprv, &sameState,*/ Neighbors, subsize[0], subsize[1], Corners, Upper, Down, Left, Right);
            
        
        // if(numOfN == N){
        //     MPI_Allreduce(&currState, &mpiReduceRes, 1, MPI_INT, MPI_SUM, cartcomm);
        //     if(mpiReduceRes == 0){
        //         break;
        //     }
        //     numOfN = 0;
        // }
        // numOfN++;
    }


    t2 = MPI_Wtime(); 
    
    MPI_Finalize();
    free2darray(&subLife);
    if(rank==0){
        printf( "Elapsed time is %f\n", t2 - t1 ); 
    	free2darray(&Life);
    	free2darray(&Neighbors);
    	//fclose(fp);
    }
    free(Corners);
    free(Upper);
    free(Right);
    free(Left);
    free(Down);
    return 0;
}




//skaei to rank 1
//na mazeuw ta currstate kai na vriskw an einai 0 o pinakas
//na kanw to idio gia sameState