#include <stdio.h>
#include <math.h>
#include "life.h"
#include "Functions.h"
#include "mpi.h"

#define SIZEN 100
#define SIZEM 100 
#define UP    0
#define DOWN  1
#define LEFT  2
#define RIGHT 3



int main(int argc, char **argv){
	int a=0, b=0;
	int i, numprocs, rank;
	int currentstate = 0, previewsstate = 0;
	FILE *fp;
	MPI_Status recv_status, send_status;
	MPI_Request request;
	int tag = 23; /* arbitrary value */


	MPI_Init(&argc, &argv);    //initializing mpi
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);    //getting the number of processes
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);    //getting the id of each process
	printf("numprocs is %d\n", numprocs);


	struct organism org[SIZEN][SIZEM];
	
	double sqrtrslt = sqrt(numprocs);
	sqrtrslt = ceil(sqrtrslt);
	int dims[2]={sqrtrslt,sqrtrslt};  //orizetai me vash to numprocs
	int periods[2]={0,0}, reorder=1, coords[2], inbuf[8]={MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL};
	int nbrs[8];
	int source, dest, outbuf;

	MPI_Request reqs[8];
   	MPI_Status stats[8];
   	MPI_Comm cartcomm;

		

	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &cartcomm);
    MPI_Comm_rank(cartcomm, &rank);
    MPI_Cart_coords(cartcomm, rank, 2, coords);
    MPI_Cart_shift(cartcomm, 0, 1, &nbrs[UP], &nbrs[DOWN]);
    MPI_Cart_shift(cartcomm, 1, 1, &nbrs[LEFT], &nbrs[RIGHT]);

    printf("rank= %d coords= %d %d  neighbors(u,d,l,r)= %d %d %d %d\n",
    rank,coords[0],coords[1],nbrs[UP],nbrs[DOWN],nbrs[LEFT],
    nbrs[RIGHT]);

    outbuf = rank;

    MPI_Waitall(8, reqs, stats);

/******ws edw ftiaxnetai h topologia***********/
    //

	if(rank==0){  //master process

		fp = Openfile(); //opening the file
		if (!fp)
		{
			printf("error opening file\n");
			MPI_Finalize();jh
			return -1;
		}
		
		for (i = 0; i < (SIZEN*SIZEM); i++) {
			fscanf(fp, "%d ", &org[a][b].life);		//read from file to array
			
			if(b!=0 && b % (SIZEM-1) == 0){
				b=0;
				a++;
			}
			else{
				b++;
			}
		}


		// exchange data (rank) with 4 neighbors
		for (i=0; i<4; i++) {
    		dest = nbrs[i];
        	source = nbrs[i];
        	MPI_Isend(&outbuf, 1, MPI_INT, dest, tag, MPI_COMM_WORLD, &reqs[i]);
        	MPI_Irecv(&inbuf[i], 1, MPI_INT, source, tag, MPI_COMM_WORLD, &reqs[i+4]);
    	}
	}

	
	do{
		currentstate=0;
		a = 0;
		b = 0;

		for (i = 0; i < SIZEN*SIZEM; i++) {		//calculate neighbors
			
			if(a==0){
				if(b==0){		//upper-left
					org[a][b].neighbors = org[a][b+1].life + org[a+1][b].life + org[a+1][b+1].life;
				}
				else if(b%(SIZEM-1)==0){		//upper-right
					org[a][b].neighbors = org[a][b-1].life + org[a+1][b-1].life + org[a+1][b].life;
				}
				else{			//upper-middle
					org[a][b].neighbors = org[a][b-1].life + org[a][b+1].life + org[a+1][b-1].life + org[a+1][b].life + org[a+1][b+1].life;
				}
			}
			else if(a%(SIZEN-1)==0){
				if(b==0){		//down-left
					org[a][b].neighbors = org[a-1][b].life + org[a-1][b+1].life + org[a][b+1].life;
				}
				else if(b%(SIZEM-1)==0){		//down-right
					org[a][b].neighbors = org[a-1][b-1].life + org[a-1][b].life + org[a][b-1].life;
				}
				else{			//down-middle
					org[a][b].neighbors = org[a-1][b-1].life + org[a-1][b].life + org[a-1][b+1].life + org[a][b-1].life + org[a][b+1].life;
				}
			}
			else{
				if(b==0){		//center-left
					org[a][b].neighbors = org[a-1][b].life + org[a-1][b+1].life + org[a][b+1].life + org[a+1][b].life + org[a+1][b+1].life;
				}
				else if(b%(SIZEM-1)==0){		//center-right
					org[a][b].neighbors = org[a-1][b-1].life + org[a-1][b].life + org[a][b-1].life + org[a+1][b-1].life + org[a+1][b].life;
				}
				else{			//center-middle
					org[a][b].neighbors = org[a-1][b-1].life + org[a-1][b].life + org[a-1][b+1].life + org[a][b-1].life + org[a][b+1].life + org[a+1][b-1].life + org[a+1][b].life + org[a+1][b+1].life;
				}
			}
				
			if(b!=0 && b % (SIZEN-1) == 0){		//traverse pointers
				b=0;
				a++;
			}
			else{
				b++;
			}
		}
		a = 0;
		b = 0;
		for (i = 0; i < SIZEN*SIZEM; i++) {
			
			if ( org[a][b].life == 1 && org[a][b].neighbors <= 1){	//1st rule
				org[a][b].life = 0;
				currentstate =1;
			}
			
			if ( org[a][b].life == 1 && org[a][b].neighbors >= 4){	//3rd rule
				org[a][b].life = 0;
				currentstate =1;
			}
			
			if ( org[a][b].life == 0 && org[a][b].neighbors == 3){	//4th rule
				org[a][b].life = 1;
				currentstate =1;
			}
			
			if(b!=0 && b % (SIZEM-1) == 0){
				b=0;
				a++;
			}
			else{
				b++;
			}
		}
		if(currentstate==1){  //elegxos an ola einai 0
			int sum=0;
			a = 0;
			b = 0;
			for (i = 0; i < SIZEN*SIZEM; i++) {
				sum = sum + org[a][b].life;

				if(b!=0 && b % (SIZEM-1) == 0){
					b=0;
					a++;
				}
				else{
					b++;
				}
			}
			if(sum==0){
				currentstate=0;
			}
		}

	}while(currentstate == 0);

	a = 0;
	b = 0;
	for (i = 0; i < SIZEN*SIZEM; i++) {
		printf("%d ", org[a][b].neighbors);
		if(b!=0 && b % (SIZEM-1) == 0){
			b=0;
			a++;
		}
		else{
			b++;
		}
	}
	
	
	MPI_Finalize();
	
	fclose(fp);
	return 0;
}
