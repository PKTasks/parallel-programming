flags = -g -c
source =  Functions.c main.c
objs = Functions.o main.o
headers = life.h Functions.h
exe = life
cc = gcc

all: $(objs)
	$(cc) -g $(objs) -lm -o $(exe)

Functions.o: Functions.c
	$(cc) $(flags) Functions.c

main.o: main.c
	$(cc) $(flags) main.c

gdb:
	gdb ./life

clean:
	rm -f $(objs) $(exe)
 
